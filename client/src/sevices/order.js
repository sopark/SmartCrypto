import axios from 'axios';

export function postBuying(privateKey, coinName, type, unitPrice, volume) {
    let data = JSON.stringify({
        privateKey: privateKey,
        coinName: coinName,
        transactionType: type,
        qty: volume,
        price: unitPrice
    });

    axios.post('http://localhost:8080/order', data, {
            headers: {
                'Content-Type' : 'application/json'
            }
        })
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        });
}

export function postSelling(privateKey, coinName, type, unitPrice, volume) {
    let data = JSON.stringify({
        privateKey: privateKey,
        coinName: coinName,
        type: type,
        volume: volume,
        unitPrice: unitPrice
    });

    axios.post('http://localhost:8080/order', data, {
            headers: {
                'Content-Type' : 'application/json'
            }
        })
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        });
}