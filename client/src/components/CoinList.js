import React, {Component} from 'react';
import '../App.css';

class CoinList extends Component {
    render() {
        return (
            <div>
                <div className="exchange-list">
                    <table cellPadding="5" cellSpacing="2" align="center" className="exchange-table">
                        <thead>
                        <tr>
                            <th>코인명</th>
                            <th>현재가격</th>
                            <th>거래대금</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr className="exchange-row">
                                <td className="name">ERC</td>
                                <td className="current-price">50,000</td>
                                <td className="total-money">1,000,000</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default CoinList;