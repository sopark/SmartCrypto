import React, {Component} from 'react';
import '../App.css';
import ExchangeItem from './ExchangeItem'

class RequestList extends Component {
    render() {
        return (
            <div>
                <div className="exchange-list">
                    <table cellPadding="5" cellSpacing="2" align="center" className="exchange-table">
                        <thead>
                        <tr>
                            <th>이름</th>
                            <th>가격</th>
                            <th>종류</th>
                            <th>수량</th>
                        </tr>
                        </thead>
                        <tbody>
                            {this.props.requests.map(
                                (item, index) => {
                                    return( <ExchangeItem
                                        isChange = {item.isChange}
                                        coinName = {item.coinName}
                                        kind={item.type}
                                        count={item.volume}
                                        price={item.unitPrice}
                                        key={index}
                                    />);
                                }
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
}

export default RequestList;