import React from 'react';
import '../App.css';

const BuyingForm = ({kind, price, count, onChange, onCreate}) => {
    return (
        <div className="form">
            <div>
                <span>가격 : </span>
                <input onChange={onChange} className="buyingPrice"  name="buyingPrice" value={price} />
            </div>
            <div>
                <span>수량 : </span>
                <input onChange={onChange} className="buyingCount" name="buyingCount" value={count} />
            </div>
            <div name="BUY" className="create-button" onClick={onCreate}>
                매수하기
            </div>
        </div>
    );
};

export default BuyingForm;