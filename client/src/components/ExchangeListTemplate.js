import React from 'react';
import '../App.css';
import CoinList from "./CoinList";

const ExchangeListTemplate = ({pForm, sForm, exchanges, requests}) => {
    return (
        <main>
            <div className="current-board">
                <div className="list-template coin-list">
                    <div className="title">
                        코인 종류
                    </div>
                    <section className="exchanges-wrapper completeList">
                        <CoinList/>
                    </section>
                </div>
                <div className="list-template complete-list">
                    <div className="title">
                        체결내역
                    </div>
                    <section className="exchanges-wrapper completeList">
                        {exchanges}
                    </section>
                </div>
                <div className="list-template call-list">
                    <div className="title">
                        호가
                    </div>
                    <section className="exchanges-wrapper completeList">
                        {requests}
                    </section>
                </div>
            </div>

            <div className="exchange-forms">
                <div className="exchange-form pForm">
                    <div className="title">
                        매수
                    </div>
                    <section className="form-wrapper">
                        {pForm}
                    </section>
                </div>
                <div className="exchange-form sForm">
                    <div className="title">
                        매도
                    </div>
                    <section className="form-wrapper">
                        {sForm}
                    </section>
                </div>
            </div>

        </main>
    );
};

export default ExchangeListTemplate;