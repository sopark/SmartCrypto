import React from 'react';
import logo from '../logo_main.png';
import '../App.css';

class Header extends React.Component {
    render() {
        return (
            <div>
                <div className="nav-bar">
                    <img src={logo} alt={"logo"} height="100%"/>
                </div>
            </div>
        );
    }
}

export default Header;