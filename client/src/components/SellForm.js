import React from 'react';
import '../App.css';

const SellForm = ({kind, price, count, onChange, onCreate}) => {
    return (
        <div className="form">
            <div>
                <span>가격 : </span>
                <input onChange={onChange} className="sellPrice" name="sellPrice" value={price} />
            </div>
            <div>
                <span>수량 : </span>
                <input onChange={onChange} className="sellCount" name="sellCount" value={count} />
            </div>
            <div name="SELL" className="create-button" onClick={onCreate}>
                매도하기
            </div>
        </div>
    );
};

export default SellForm;