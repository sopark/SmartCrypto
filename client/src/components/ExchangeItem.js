import React, {Component} from 'react';
import '../App.css';

class ExchangeItem extends Component {
    render() {
        const trStyle = (this.props.kind === 'buy') ? {color: 'red'} : {color: 'blue'};
        const newItemStyle = (this.props.isChange === 'true') ? {backgroundColor: '#f0f7f7'} : {backgroundColor: 'transparent'};

        return (
            <tr style={Object.assign(trStyle, newItemStyle)} className="exchange-row">
                <td className="coinName">{this.props.coinName}</td>
                <td className="price">{this.props.price}</td>
                <td className="kind">{(this.props.kind === 'buy') ? "매수" : "매도"}</td>
                <td className="count">{this.props.count}</td>
            </tr>
        );
    }
}

export default ExchangeItem;
