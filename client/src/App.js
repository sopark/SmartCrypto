import React, {Component} from 'react';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import './App.css';
import ExchangeListTemplate from './components/ExchangeListTemplate';
import PurchaseForm from './components/BuyingForm';
import ExchangeList from './components/ExchangeList';
import RequestList from './components/RequestList';
import SellForm from "./components/SellForm";
import Header from "./components/Header";
import * as service from "./sevices/order"

const socket = new SockJS('http://localhost:8200/smart_crypto');
const stompClient = Stomp.over(socket);

class App extends Component {
    id = 1;

    constructor(props) {
        super(props);
        this.state = {
            stompClient,
            buyingPrice: 0,
            buyingCount: 0,
            sellPrice: 0,
            sellCount: 0,
            exchanges: [],
            requests: [],
            coinName: "ERC"
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleCreate = this.handleCreate.bind(this);
    }

    setupWebSocket = () => {
        const self = this;
        const webSoc = this.state.stompClient;
        webSoc.connect({}, function (frame) {
            console.log('Connected: ' + frame);
            //실제 체결내역
            webSoc.subscribe('/topic/conclusion', function (greeting) {
                const newExchange = JSON.parse(JSON.parse(greeting.body).msg);
                self.setState({ exchanges : newExchange });
            });
            //호가
            webSoc.subscribe('/topic/price', function (greeting) {
                const newRequest = JSON.parse(JSON.parse(greeting.body).msg);
                self.setState({ requests : newRequest });
            });
        });
    }

    handleChange = (e) => {
        const target = e.target.name;
        switch (target) {
            case "buyingPrice" :
                this.setState({buyingPrice: e.target.value});
                break;
            case "buyingCount" :
                this.setState({buyingCount: e.target.value});
                break;
            case "sellPrice" :
                this.setState({sellPrice: e.target.value});
                break;
            case "sellCount" :
                this.setState({sellCount: e.target.value});
                break;
            default :
                break
        }
    }

    handleCreate = (e) => {
        const kind = e.target.getAttribute('name');
        switch (kind) {
                case "BUY" :
                service.postBuying("privateKey", this.state.coinName, kind, this.state.buyingPrice, this.state.buyingCount);
                this.setState({
                    buyingPrice: 0,
                    buyingCount: 0
                });
                break;
            case "SELL" :
                service.postSelling("privateKey", this.state.coinName, kind, this.state.sellPrice, this.state.sellCount);
                this.setState({
                    sellPrice : 0,
                    sellCount : 0
                });
                break;
            default :
                break
        }
    }

    componentDidMount() {
        this.setupWebSocket();
    }

    render() {
        return (
            <div id="daou-bit">
                <Header/>
                <ExchangeListTemplate
                    pForm={(
                        <PurchaseForm
                            price={this.state.buyingPrice}
                            count={this.state.buyingCount}
                            onChange={this.handleChange}
                            onCreate={this.handleCreate}
                        />
                            )}
                    sForm={(
                        <SellForm
                            price={this.state.sellPrice}
                            count={this.state.sellCount}
                            onChange={this.handleChange}
                            onCreate={this.handleCreate}
                        />
                    )}
                    exchanges= {(
                        <ExchangeList
                            exchanges={this.state.exchanges}
                        />
                    )}
                    requests= {(
                        <RequestList
                            requests={this.state.requests}
                        />
                    )}
                >
                </ExchangeListTemplate>
            </div>
        );
    }
}

export default App;
