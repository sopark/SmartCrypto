# SmartCrypto

소스관리 : https://gitlab.com/sopark/SmartCrypto

![image](https://user-images.githubusercontent.com/6028071/44615101-3b528c00-a86c-11e8-93b9-66bf56395b28.png)
 

 개발 사항
- Kubernete : 상오
- WEBFLUX : 동준, 덕현, 상은
- SOCKET 통신 : 상오
- QUEUE : 동준
- 블럭체인 : 정주
- WORKER : 
- Client : 상은
- MongoDB : 동준 덕현



참고 사이트
---------------------------
React.js 
- https://velopert.com/reactjs-tutorials

WebFlux
- https://www.youtube.com/channel/UCcqH2RV1-9ebRBhmN_uaSNg

Infura
https://infura.io/

분산거래소
https://medium.com/@eunej/2018-%ED%83%88%EC%A4%91%EC%95%99%ED%99%94-%EA%B1%B0%EB%9E%98%EC%86%8C-%EC%83%9D%ED%83%9C%EA%B3%84-%EB%B0%8F-%ED%98%84%ED%99%A9-80b3ef795e08

발표자료PPT
https://docs.google.com/presentation/d/1yaoHwX4OJBkN5oUQ9cYAXbQWvuhUEk-D_8Pyn9whYQ4/edit#slide=id.g4689a8bab2_0_60



MongoDB Connection Info
-------------------------------
*Mongo Client에서 접속시
- Address : ds125555.mlab.com
- Port : 25555
- Authentication: Database(daoubit), UserName(daoubit), Password(daoubit1!)

- 데이터베이스 : daoubit
- 계정 : daoubit, 비번 : daoubit1!



Exchange Contract 실행 순서
---------------------------------
1.ERC20 Token 컨트랙트 배포
2.Exchange Token 컨트랙트 배포
3.거래소에 발행한  “ERC” 추가(addToken) ==> “ERC”, 0x96377e6348d400026bd661ebfa24935bca4fd5c3
4.거래소에 추가된 토큰 확인(hasToken) ==> “ERC”
5.ERCToken 컨트랙트에서 각 계정을 거래하고자 하는 거래소의 스마트컨트랙트 주소에 approve시켜야 한다.  ** 매우중요 **
6. Deposit Ether(depositEther)
7. Deposit Token
8. 거래(sell, buy)
9. 호가 정보 확인
10. 


호가정보 API 
--------------------------
http://localhost:9090/api/exhange/orderBook
[{"coinName":"ERC","type":"sell","volume":"500","unitPrice":"200"},
{"coinName":"ERC","type":"sell","volume":"100","unitPrice":"300"},
{"coinName":"ERC","type":"sell","volume":"100","unitPrice":"400"},
{"coinName":"ERC","type":"buy","volume":"300","unitPrice":"100"}]


1.RequestKeeper(Port:9092)
--------------------------


2.Worker(Port:9090)
--------------------------


3.Response-Server(Port:8100)
---------------------------
* 호가정보 조회 -> Worker (Quartz에 의해 1초간격 수행)
* 호가정보 전달-> 웹소켓서버
* 체결내역 조회 -> Worker (미구현)
* 체결내역 전달-> 웹소켓서버 (미구현)


4.Websocket-Server(Port:8200)
---------------------------
* 호가정보 브로드캐스팅 -> 사용자
* EndPointName : smart_crypto
* Topic : /topic/price(호가), /topic/conclusion(체결내역-미구현)

@ /topic/price Example
[{"coinName":"ERC","type":"sell","volume":"500","unitPrice":"200"},{"coinName":"ERC","type":"sell","volume":"100","unitPrice":"300"},{"coinName":"ERC","type":"sell","volume":"100","unitPrice":"400"},{"coinName":"ERC","type":"buy","volume":"300","unitPrice":"100"}]


5.Client(Port:3000)
----------------------------

