package com.crypto.requestkeeper.test;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@SpringBootApplication
@RestController
@Slf4j
public class RequestkeeperApplication {
    static final String URL1 = "http://localhost:8082/service?req={req}";
    static final String URL2 = "http://localhost:8082/service2?req={req}";

    WebClient client = WebClient.create();

    public static void main(String[] args) {
        System.setProperty("reactor.ipc.netty.workerCount", "2");
        System.setProperty("reactor.ipc.netty.pool.maxConnections", "2000");
        SpringApplication.run(RequestkeeperApplication.class, args);
    }

    @GetMapping("/rest")
    public Mono<String> rest(int idx) {
        log.info("request call!! {}", idx);
        Mono<ClientResponse> res = client.get().uri(URL1, idx).exchange();
        Mono<String> body = res.flatMap(clientResponse -> clientResponse.bodyToMono(String.class));

        return body;
    }
}
