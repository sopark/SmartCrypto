package com.crypto.requestkeeper.application.data;

import lombok.Data;
import lombok.NonNull;

@Data
public class OrderAddCommand {
    @NonNull
    private String privateKey;
    @NonNull
    private String coinName;
    @NonNull
    private String transactionType; // 매수,매도
    @NonNull
    private int qty; //체결수량
    @NonNull
    private int price; //거래단가
}
