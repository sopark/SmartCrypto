package com.crypto.requestkeeper.api;

import com.crypto.requestkeeper.application.OrderService;
import com.crypto.requestkeeper.application.data.OrderAddCommand;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class OrderController {

    private OrderService orderService;

    @PostMapping("/order")
    public void buy(@RequestBody OrderAddCommand orderAddCommand) throws JsonProcessingException, InterruptedException {
        orderService.order(orderAddCommand);
    }
}
