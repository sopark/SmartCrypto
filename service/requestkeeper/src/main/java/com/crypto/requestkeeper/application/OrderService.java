package com.crypto.requestkeeper.application;

import com.crypto.requestkeeper.application.data.OrderAddCommand;
import com.crypto.requestkeeper.domain.Order;
import com.crypto.requestkeeper.domain.OrderRepository;
import com.crypto.requestkeeper.infra.QueueService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.concurrent.CountDownLatch;

@Service
@RequiredArgsConstructor
public class OrderService {

    @NonNull
    private OrderRepository orderRepository;

    @NonNull
    private QueueService queueService;

    @NonNull
    private ObjectMapper objectMapper;

    public void order(OrderAddCommand command) throws JsonProcessingException {
        orderRepository.save(new Order(command.getPrivateKey(), command.getCoinName(), Order.TransactionType.BUY, command.getQty(), command.getPrice()));
        queueService.send("order", objectMapper.writeValueAsString(command));
    }
}
