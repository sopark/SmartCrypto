package com.crypto.requestkeeper.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Table(name = "orders")
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Order {

    @Id
    @GeneratedValue
    private Long id;
    @NonNull
    private String privateKey;
    @NonNull
    private String coinName;
    @NonNull
    private TransactionType transactionType; // 매수,매도
    @NonNull
    private int qty; //체결수량
    @NonNull
    private int price; //거래단가

    public enum TransactionType {
        SELL("SELL"), BUY("BUY");
        String value;
        TransactionType(String value) {
            this.value = value;
        }


        public static TransactionType byValue(String value) {

            for (TransactionType status : values()) {
                if (status.value.equalsIgnoreCase(value)) {
                    return status;
                }
            }
            return null;
        }
    }
}
