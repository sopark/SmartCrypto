package com.smartcrypto.service.admin;

import java.io.IOException;

public interface Web3DeployService {
    String deployExchange();
    String deployERC20Token();
    String addTokenExchange();

    String totalSupplyERC20Token() throws Exception;
    String getVersion();
    void sendTransaction();
}
