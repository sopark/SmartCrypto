package com.smartcrypto.service.client;

import com.smartcrypto.domain.Order;
import com.smartcrypto.domain.PendingTransaction;
import reactor.core.publisher.Mono;
import rx.Observable;
import rx.Subscription;

import java.util.List;

public interface Web3TrService {
    String sellTokenExchange(PendingTransaction pt) throws Exception;
    String buyTokenExchange(PendingTransaction pt) throws Exception;
    String depositToken(String seller) throws Exception;
    String getTokenBalance(String seller) throws Exception;
    List<Order> getOrderBook() throws Exception;
}
