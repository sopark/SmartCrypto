package com.smartcrypto.service.admin;

import com.smartcrypto.config.Web3Config;
import com.smartcrypto.service.contractWrapper.ERC20Token;
import com.smartcrypto.service.contractWrapper.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthAccounts;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.ClientTransactionManager;
import org.web3j.tx.gas.DefaultGasProvider;

import java.math.BigInteger;


@Component
public class Web3DeployServiceImpl implements Web3DeployService {

    private static final Logger log = LoggerFactory.getLogger(Web3DeployServiceImpl.class.getName());

    public Web3DeployServiceImpl() {
    }

    @Override
    public String deployExchange() {
        Credentials credentials = Credentials.create("23279BBA97981F4DA1CB4CCD95C9D8DD5E98642A4CFF3613CA114BC7DF4ACFA9");
        try {
            Exchange contract = Exchange.deploy(Web3Config.web3, credentials, DefaultGasProvider.GAS_PRICE, DefaultGasProvider.GAS_LIMIT).send();
            log.debug("Exchange Contract Address : {}" , contract.getContractAddress());
        } catch (Exception e) {
            log.error("deployExchange : {}" + e.getMessage());
        }

        return "deploy";
    }

    @Override
    public String deployERC20Token() {
        Credentials credentials = Credentials.create("23279BBA97981F4DA1CB4CCD95C9D8DD5E98642A4CFF3613CA114BC7DF4ACFA9");
        try {
            ERC20Token contract = ERC20Token.deploy(Web3Config.web3, credentials, DefaultGasProvider.GAS_PRICE, DefaultGasProvider.GAS_LIMIT).send();
            log.debug("ERC20Token  Contract Address : {}" , contract.getContractAddress());
        } catch (Exception e) {
            log.error("deployERC20Token : {}" + e.getMessage());
        }
        return "deploy";
    }


    @Override
    public String addTokenExchange() {
        Credentials credentials = Credentials.create("23279BBA97981F4DA1CB4CCD95C9D8DD5E98642A4CFF3613CA114BC7DF4ACFA9");
        Exchange contract = Exchange.load(
                Web3Config.EXCHANGE_CONTRACT_ADDRESS, Web3Config.web3, credentials, DefaultGasProvider.GAS_PRICE, DefaultGasProvider.GAS_LIMIT);

        contract.addToken("DaouMoneyMoney",Web3Config.ERC20_CONTRACT_ADDRESS).observable().subscribe( receipt -> {
            System.out.println("added Token Completed!");
        });

        contract.hasToken("DaouMoneyMoney").observable().subscribe( val -> {
            System.out.println("HasToken : " + val);
        });
        return "addToken";
    }

    @Override
    public String totalSupplyERC20Token() throws Exception {
        EthAccounts accounts = Web3Config.web3.ethAccounts().send();
        String accountAddr = accounts.getAccounts().get(0);
        ClientTransactionManager transactionManager = new ClientTransactionManager(Web3Config.web3, accountAddr);
        ERC20Token contract = ERC20Token.load(
                Web3Config.ERC20_CONTRACT_ADDRESS, Web3Config.web3, transactionManager, DefaultGasProvider.GAS_PRICE, DefaultGasProvider.GAS_LIMIT);

        contract.totalSupply().observable().subscribe( supply -> {
            System.out.println("totalSupply : " + supply);
        });

        String from = "0x0d8c0ead945498ea0ef905425e8eedc0120d940c";
        String to = "0xe186daee8d09e4347388fc0bb9a5fdda3719fe1e";

        contract.balanceOf(from).observable().subscribe(balance -> System.out.println("From Balance : " + balance));
        contract.balanceOf(to).observable().subscribe(balance -> System.out.println("To Balance : " + balance));

        //contract.transfer(to, new BigInteger("300"));
        contract.allowance(from, to).send();
        contract.approve(from, BigInteger.valueOf(500000)).send();
        contract.approve(to, BigInteger.valueOf(500000)).send();
        TransactionReceipt receipt = contract.transferFrom(from, to, BigInteger.valueOf(10000)).send();

        contract.balanceOf(from).observable().subscribe(balance -> System.out.println("From Balance : " + balance));
        contract.balanceOf(to).observable().subscribe(balance -> System.out.println("To Balance : " + balance));

        return "totalSupply";
    }

    @Override
    public String getVersion() {
        Web3Config.web3.web3ClientVersion().observable().subscribe(x -> {
            String clientVersion = x.getWeb3ClientVersion();
            System.out.println("clientVersion : " + clientVersion);
        });
        return "version";
    }

    @Override
    public void sendTransaction() {
        //TODO : https://medium.com/yopiter/interfacing-with-ethereum-smart-contracts-in-java-cf39b2e95b4e
        //Using the web3j Command Line Tools(https://docs.web3j.io/command_line.html)
        //It will create a .java file with the following contents
        Web3Config.web3.ethSendTransaction(new Transaction(
                "",new BigInteger("0"),new BigInteger("0"),new BigInteger("0"),"",new BigInteger("0"),""
        )).observable().subscribe(x -> {
            System.out.println("Transaction Commited.");
        });
    }
}
