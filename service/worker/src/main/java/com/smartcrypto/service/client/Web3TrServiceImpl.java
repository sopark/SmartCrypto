package com.smartcrypto.service.client;


import com.google.common.collect.Streams;
import com.smartcrypto.config.Web3Config;
import com.smartcrypto.domain.Order;
import com.smartcrypto.domain.PendingTransaction;
import com.smartcrypto.repository.PendingTransactionRepository;
import com.smartcrypto.service.contractWrapper.Exchange;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;
import org.web3j.crypto.Credentials;
import org.web3j.tx.gas.DefaultGasProvider;

import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import rx.Observable;
import rx.Subscription;

import java.math.BigInteger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * https://docs.web3j.io/infura.html
 * Main Ethereum Network:
 * https://mainnet.infura.io/<your-token>
 * Test Ethereum Network (Rinkeby):
 * https://rinkeby.infura.io/<your-token>
 * Test Ethereum Network (Kovan):
 * https://kovan.infura.io/<your-token>
 * Test Ethereum Network (Ropsten):
 * https://ropsten.infura.io/<your-token>
 *
 * Web3j web3 = Web3j.build(new HttpService("https://rinkeby.infura.io/<your-token>"));
 * Web3ClientVersion web3ClientVersion = web3.web3ClientVersion().send();
 * System.out.println(web3ClientVersion.getWeb3ClientVersion());
 *
 *
 * */
@Component
public class Web3TrServiceImpl implements Web3TrService {

    private static final Logger log = LoggerFactory.getLogger(Web3TrServiceImpl.class.getName());

    @Autowired
    private PendingTransactionRepository pendingTransactionRepository;

    private Credentials adminCredentials;
    private Exchange adminContract;

    public Web3TrServiceImpl() {
            //TODO : Admin Contract
            adminCredentials = Credentials.create(Web3Config.ADMIN_PRIVATE_KEY);
            adminContract = Exchange.load(
                Web3Config.EXCHANGE_CONTRACT_ADDRESS, Web3Config.web3, adminCredentials, DefaultGasProvider.GAS_PRICE, DefaultGasProvider.GAS_LIMIT);
    }

    @Override
    public String sellTokenExchange(PendingTransaction pt) {
        Credentials credentials = null;
        if(StringUtils.isEmpty(pt.getAccountAddress())) {
            credentials = Credentials.create("23279BBA97981F4DA1CB4CCD95C9D8DD5E98642A4CFF3613CA114BC7DF4ACFA9");
        }else {
            credentials = Credentials.create(pt.getAccountAddress());
        }

        //Contract GasPrice : 22_000_000_000L , Contract GasLimit : 4_300_000
        Exchange contract = Exchange.load(Web3Config.EXCHANGE_CONTRACT_ADDRESS, Web3Config.web3, credentials, DefaultGasProvider.GAS_PRICE, DefaultGasProvider.GAS_LIMIT);

        contract.sellToken("ERC", BigInteger.valueOf(Long.parseLong(pt.getPrice())), BigInteger.valueOf(Long.parseLong(pt.getQty()))).observable().subscribe(x -> {
            //Pending Transaction Result => MongoDB
            pt.setGasFee(x.getGasUsed().toString());
            pt.setTransactionHash(x.getTransactionHash());
            pendingTransactionRepository.save(pt);
            log.info("Web3 sellToken Transaction completed. Transaction Hash : {}", x.getTransactionHash());
        });

        return "sellToken";
    }



    @Override
    public String buyTokenExchange(PendingTransaction pt) {
        Credentials credentials = null;
        if(StringUtils.isEmpty(pt.getAccountAddress())) {
            credentials = Credentials.create("E1CA2BA0DA38175DEDA37FB4D66DF7B0EDA34409A5C3524C0C6E556067B7E541");
        }else {
            credentials = Credentials.create(pt.getAccountAddress());
        }
        Exchange contract = Exchange.load(Web3Config.EXCHANGE_CONTRACT_ADDRESS, Web3Config.web3, credentials, DefaultGasProvider.GAS_PRICE, DefaultGasProvider.GAS_LIMIT);

        contract.sellToken("ERC", BigInteger.valueOf(Long.parseLong(pt.getPrice())), BigInteger.valueOf(Long.parseLong(pt.getQty()))).observable().subscribe(x -> {
            //Pending Transaction Result => MongoDB
            pt.setGasFee(x.getGasUsed().toString());
            pt.setTransactionHash(x.getTransactionHash());
            pendingTransactionRepository.save(pt);
            log.info("Web3 buyToken Transaction completed. Transaction Hash : {}", x.getTransactionHash());
        });

        return "buyToken";
    }




    @Override
    public String depositToken(String seller) {
        Credentials credentials = null;
        if(StringUtils.isEmpty(seller)) {
            credentials = Credentials.create("23279BBA97981F4DA1CB4CCD95C9D8DD5E98642A4CFF3613CA114BC7DF4ACFA9");
        }else {
            credentials = Credentials.create(seller);
        }

        Exchange contract = Exchange.load(Web3Config.EXCHANGE_CONTRACT_ADDRESS, Web3Config.web3, credentials, DefaultGasProvider.GAS_PRICE, DefaultGasProvider.GAS_LIMIT);

        contract.depositToken("ERC", BigInteger.valueOf(Long.parseLong("100"))).observable().subscribe(x -> {
            x.getGasUsed();
            System.out.println("depositToken Transaction Compledted.");
        });

        return "depositToken";
    }

    @Override
    public String getTokenBalance(String seller) {
        Credentials credentials = null;
        if(StringUtils.isEmpty(seller)) {
            credentials = Credentials.create("23279BBA97981F4DA1CB4CCD95C9D8DD5E98642A4CFF3613CA114BC7DF4ACFA9");
        }else {
            credentials = Credentials.create(seller);
        }
        Exchange contract = Exchange.load(Web3Config.EXCHANGE_CONTRACT_ADDRESS, Web3Config.web3, credentials, DefaultGasProvider.GAS_PRICE, DefaultGasProvider.GAS_LIMIT);
        contract.getTokenBalance("ERC").observable().subscribe( x -> {
            System.out.println("Token Balance : " + x);
        });

        return "tokenBalance";
    }


    public List<Order> getOrderBook() {
        List<Order> orders = new ArrayList<>();

        adminContract.getSellOderBook("ERC").observable().subscribe(sellTuple -> {

            List<BigInteger> sellPrices = sellTuple.getValue1();
            List<BigInteger> sellQty = sellTuple.getValue2();

            Streams.zip(sellPrices.stream(), sellQty.stream(), (price, qty) -> {
                return new Order("ERC", "sell", qty.toString(), price.toString());
            }).collect(Collectors.toList()).forEach(order -> {
                orders.add(order);
            });


            adminContract.getBuyOrderBook("ERC").observable().subscribe( buyTuple -> {
                List<BigInteger> buyPrices = buyTuple.getValue1();
                List<BigInteger> buyQty = buyTuple.getValue2();

                Streams.zip(buyPrices.stream(), buyQty.stream(), (price, qty) -> {
                    return new Order("ERC", "buy", qty.toString(), price.toString());
                }).collect(Collectors.toList()).forEach(order -> {
                    orders.add(order);
                });
            });
        });

        return orders;
    }

}
