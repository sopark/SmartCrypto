package com.smartcrypto.repository;

import com.smartcrypto.domain.PendingTransaction;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PendingTransactionRepository extends MongoRepository<PendingTransaction, String> {
}
