package com.smartcrypto.controller;

import com.smartcrypto.service.admin.Web3DeployService;
import com.smartcrypto.service.client.Web3TrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;


/**
 * TODO : 향후 테스트용, 우선 보류
 * */
@RestController
public class OrderAdminController {
    
    @Autowired
    private Web3DeployService web3DeployService;
    
    @Autowired
    private Web3TrService web3TrService;

    @GetMapping("web3j/version")
    public Mono getVersion() throws Exception {
        return Mono.just(web3DeployService.getVersion());
    }

    @GetMapping("web3j/token/deploy")
    public Mono deployToken() throws Exception {
        return Mono.just(web3DeployService.deployERC20Token());
    }


    @GetMapping("web3j/exchange/deploy")
    public Mono deployExchange() throws Exception {
        return Mono.just(web3DeployService.deployExchange());
    }


    @GetMapping("web3j/token/totalSupply")
    public Mono load() throws Exception {
        return Mono.just(web3DeployService.totalSupplyERC20Token());
    }


    @GetMapping("web3j/exchange/depositToken")
    public Mono depositTokenExchange() throws Exception {
        return Mono.just(web3TrService.depositToken(null));
    }


    @GetMapping("web3j/exchange/addToken")
    public Mono addTokenExchange() throws Exception {
        return Mono.just(web3DeployService.addTokenExchange());
    }

    @GetMapping("web3j/exchange/balance")
    public Mono balanceTokenExchange() throws Exception {
        return Mono.just(web3TrService.getTokenBalance(null));
    }

    //TODO : 매도자의 privateKey를 받아야함.
    @GetMapping("web3j/exchange/sellToken")
    public Mono sellTokenExchange() throws Exception {
        return Mono.just(web3TrService.sellTokenExchange(null));
    }

    //TODO : 매수자의 privateKey를 받아야함.
    @GetMapping("web3j/exchange/buyToken")
    public Mono buyTokenExchange() throws Exception {
        return Mono.just(web3TrService.buyTokenExchange(null));
    }
}