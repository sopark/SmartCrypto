package com.smartcrypto.controller.api;

import com.smartcrypto.domain.Order;
import com.smartcrypto.service.client.Web3TrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 외부 API
 * */
@RestController
public class OrderApiController {

    @Autowired
    private Web3TrService web3TrService;

    @GetMapping("/api/exhange/orderBook")
    public List<Order> orderBook() throws Exception {
        return web3TrService.getOrderBook();
    }
}