package com.smartcrypto.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smartcrypto.config.KafkaConfig;
import com.smartcrypto.domain.PendingTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ConverterFactory {

    static final Logger log = LoggerFactory.getLogger(KafkaConfig.class.getName());

    public static PendingTransaction convertPendingTranction(String msg) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(msg, PendingTransaction.class);
        } catch (IOException e) {
            log.error("Kafka Msg Parsing Exception : {}", e.getMessage());
            return null;
        }
    }

}
