package com.smartcrypto.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Order {
    private String coinName;
    private String type; // 매수,매도
    private String volume; //수량
    private String unitPrice; //거래단가

    public Order(String coinName, String type, String volume, String unitPrice) {
        this.coinName = coinName;
        this.type = type;
        this.volume = volume;
        this.unitPrice = unitPrice;
    }

    public String getCoinName() {
        return coinName;
    }

    public void setCoinName(String coinName) {
        this.coinName = coinName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }
}
