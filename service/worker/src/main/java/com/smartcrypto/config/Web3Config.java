package com.smartcrypto.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;

@Configuration
public class Web3Config {
    public static final String ERC20_CONTRACT_ADDRESS = "0x96377e6348d400026bd661ebfa24935bca4fd5c3";
    public static final String EXCHANGE_CONTRACT_ADDRESS = "0x76d5ebb1995e1e2010a6426b799ac9aeb64ef68e";
    public static final String ADMIN_PRIVATE_KEY = "23279BBA97981F4DA1CB4CCD95C9D8DD5E98642A4CFF3613CA114BC7DF4ACFA9";

    private static final Logger log = LoggerFactory.getLogger(KafkaConfig.class.getName());

    //Robsten 테스트넷
    public static final Web3j web3 = Web3j.build(new HttpService("https://ropsten.infura.io/v3/ae070b9296954078874dfe84f3154d40"));;

    public Web3Config() {}
}
