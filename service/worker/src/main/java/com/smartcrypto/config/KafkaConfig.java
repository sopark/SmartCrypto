package com.smartcrypto.config;

import com.smartcrypto.converter.ConverterFactory;
import com.smartcrypto.domain.PendingTransaction;
import com.smartcrypto.service.client.Web3TrService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;


@EnableKafka
@Configuration
public class KafkaConfig {
    private static final Logger log = LoggerFactory.getLogger(KafkaConfig.class.getName());
    private static final String TOPIC_SELL = "SELL";
    private static final String TOPIC_BUY = "BUY";

    @Autowired
    private Web3TrService web3TrService;


    @KafkaListener(topics = "order")
    public void receiveTopic(ConsumerRecord<?, ?> consumerRecord) {
        log.info("Receiver on topic: {}" + consumerRecord.topic());
        PendingTransaction pt = ConverterFactory.convertPendingTranction(consumerRecord.value().toString());

        if(pt == null) log.error("Transaction Msg Exception.");
        //TODO : Message에서 계정 정보 Get
        try {
            if (TOPIC_SELL.equals(pt.getTransactionType())) {
                web3TrService.sellTokenExchange(pt);
            } else if (TOPIC_BUY.equals(pt.getTransactionType())) {
                web3TrService.buyTokenExchange(pt);
            }
        } catch(Exception e) {
            log.error("Kafka Consumer Error : {}" , e.getMessage());
        }
    }
}
