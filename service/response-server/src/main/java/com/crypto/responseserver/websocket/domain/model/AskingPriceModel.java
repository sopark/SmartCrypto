package com.crypto.responseserver.websocket.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AskingPriceModel {
	/**
	 * 가상화폐명
	 */
	private String currencyName;
	
	/**
	 * 매수/매도 타입
	 */
	private String kind;
	
	/**
	 * 호가(가격)
	 */
	private String price;
	
	/**
	 * 요청수량
	 */
	private String count;
}
