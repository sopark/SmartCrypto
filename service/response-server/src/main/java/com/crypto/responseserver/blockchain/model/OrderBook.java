package com.crypto.responseserver.blockchain.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.crypto.responseserver.blockchain.model.Order.ChangeType;
import com.crypto.responseserver.util.SafeCodeUtil;

import lombok.Getter;

public class OrderBook {

	private final String ORDER_TYPE_SELL = "sell";
	private final String ORDER_TYPE_BUY = "buy";
	
	@Getter
	private List<Order> orders;
	
	public OrderBook(List<Order> orders) {
		this.orders = orders != null ? orders : new ArrayList<>();
		this.sort();
	}
	
	private void sort() {
		List<Order> sortedOrders = new ArrayList<>();
		sortedOrders.addAll(this.extractSellOrders()
				.stream()
				.sorted(( e1, e2 ) -> desc(e1.getUnitPrice(), e2.getUnitPrice()))
				.collect(Collectors.toList()));
		sortedOrders.addAll(this.extractBuyOrders()
				.stream()
				.sorted(( e1, e2 ) -> asc(e1.getUnitPrice(), e2.getUnitPrice()))
				.collect(Collectors.toList()));
		this.orders = sortedOrders;
	}
	
	public List<Order> extractSellOrders() {
		return this.orders.stream()
				.filter(order -> ORDER_TYPE_SELL.equalsIgnoreCase(order.getType())).collect(Collectors.toList());
	}
	
	public List<Order> extractBuyOrders() {
		return this.orders.stream()
				.filter(order -> ORDER_TYPE_BUY.equalsIgnoreCase(order.getType())).collect(Collectors.toList());
	}
	
	private int asc(int p1, int p2) {
		return (p1==p2) ? 0 : (p1>p2) ? 1 : -1;
	}
	
	private int desc(int p1, int p2) {
		return (p1==p2) ? 0 : (p1<p2) ? 1 : -1;
	}
	
	public void setIsChange(OrderBook prevOrderBook) {
		this.orders = this.orders.stream().map(order -> {
			Order prevOrder = prevOrderBook.findOrderByUnitPrice(order.getUnitPrice());
			if(prevOrder==null) {
				order.setChangeType(ChangeType.ADD);
			} else {
				order.setChangeType(prevOrder.getVolume().equals(order.getVolume()) ? ChangeType.NONE : ChangeType.MOD);
			}
			return order;
		}).collect(Collectors.toList());
	}
	
	private Order findOrderByUnitPrice(Integer unitPrice) {
		for(Order order : this.orders) {
			if(SafeCodeUtil.equals(unitPrice, order.getUnitPrice())) {
				return order;
			}
		}
		return null;
	}
}
