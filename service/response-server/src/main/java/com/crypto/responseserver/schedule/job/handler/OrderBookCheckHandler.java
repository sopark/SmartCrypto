package com.crypto.responseserver.schedule.job.handler;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crypto.responseserver.blockchain.service.OrderCheckService;
import com.crypto.responseserver.schedule.job.QuartzJobHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class OrderBookCheckHandler implements QuartzJobHandler {
	
	@Autowired
	private OrderCheckService orderCheckService;

    @Override
    public void execute(Date firedTime) {
    	log.info("[Quartz] {} executed...", this.getClass().getSimpleName());
    	orderCheckService.boradcastOrderBookToWebsocketServer();
    }

}