package com.crypto.responseserver.schedule.config;

import java.io.IOException;
import java.util.Properties;

import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import com.crypto.responseserver.schedule.job.QuartzJob;

@Configuration
public class QuartzInitializer {
    
    @Autowired
    private ApplicationContext applicationContext;
    
    @Autowired
    private SchedulerFactoryBean schedulerFactoryBean;

    @EventListener(ApplicationReadyEvent.class)
    public void initialize() throws Exception {
		Scheduler scheduler = createAndStartScheduler();
		fireJob(scheduler, QuartzJob.class);
    }

    @Bean
    public SchedulerFactoryBean schedulerFactoryBean() throws IOException {
        SchedulerFactoryBean factory = new SchedulerFactoryBean();
        factory.setOverwriteExistingJobs(true);
        factory.setQuartzProperties(quartzProperties());
        
        AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);
        factory.setJobFactory(jobFactory);
        return factory;
    }
    
    @Bean
    public Properties quartzProperties() throws IOException {
        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("/quartz.properties"));
        propertiesFactoryBean.afterPropertiesSet();
        return propertiesFactoryBean.getObject();
    }
 
	private Scheduler createAndStartScheduler() throws SchedulerException {
		Scheduler scheduler = schedulerFactoryBean.getScheduler();
		scheduler.start();
		return scheduler;
	}

	private <T extends Job> void fireJob(Scheduler scheduler, Class<T> jobClass)
			throws SchedulerException, InterruptedException {
		JobBuilder jobBuilder = JobBuilder.newJob(jobClass);
		JobDataMap data = new JobDataMap();
		data.put("latch", this);

		JobDetail jobDetail = jobBuilder
				.usingJobData("OrderJob", "com.crypto.responseserver.QuartzInitializer")
				.usingJobData(data).build();

		SimpleScheduleBuilder.simpleSchedule();
		Trigger trigger = TriggerBuilder
				.newTrigger()
				.startNow()
				.withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(2))
				.withDescription("MyTrigger").build();

		scheduler.scheduleJob(jobDetail, trigger);
	}
}