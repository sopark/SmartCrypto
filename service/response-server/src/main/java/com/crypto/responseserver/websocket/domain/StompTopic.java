package com.crypto.responseserver.websocket.domain;

import lombok.Getter;

public enum StompTopic {
	
	SAMPLE("/sample"),
	PRICE_LIST("/price"), // 호가 리스트
	CONCLUSION_LIST("/conclusion"); // 체결내역 리스트
	
	public static final String TOPIC_PREFIX = "/topic";
	
	@Getter
	private String postFix;
	
	private StompTopic(String postFix) {
		this.postFix = postFix;
	}
	
	public String getDestination() {
		return TOPIC_PREFIX + this.getPostFix();
	}
}
