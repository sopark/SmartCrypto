package com.crypto.responseserver.blockchain.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.crypto.responseserver.RemoteServerConfig;
import com.crypto.responseserver.RemoteServerConfig.WorkerApi;
import com.crypto.responseserver.blockchain.model.Order;
import com.crypto.responseserver.blockchain.model.OrderBook;
import com.crypto.responseserver.core.component.RestManager;
import com.crypto.responseserver.websocket.component.WebsocketMsgManager;
import com.crypto.responseserver.websocket.domain.StompTopic;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class OrderCheckService {

	@Autowired
	RestManager restManager;
	
	@Autowired
	WebsocketMsgManager websocketManager;
	
	private OrderBook currentOrderBook;
	
	@PostConstruct
	private void initCurrentOrderBook() {
		currentOrderBook = null;
	}
	
	public void boradcastOrderBookToWebsocketServer() {
		updateCurrentOrderbook();
		String orderbook = convertOrdersToJson(this.currentOrderBook.getOrders());
		log.info("[{}] boradcastOrderBookToWebsocketServer() -> orderbook:{}", this.getClass().getSimpleName(), orderbook);
		websocketManager.sendMsg(StompTopic.PRICE_LIST, orderbook);
	}
	
	private void updateCurrentOrderbook() {
		ResponseEntity<String> response = restManager.get(
				RemoteServerConfig.getWorkerUri(WorkerApi.GET_ORDER_BOOK), null, String.class);
		log.info("[{}] getOrderBook() -> Response:{}", this.getClass().getSimpleName(), response);
		
		OrderBook newOrderbook = new OrderBook(convertJsonToOrders(response.getBody()));
		if(currentOrderBook != null) {
			newOrderbook.setIsChange(currentOrderBook);
		}
		this.currentOrderBook = newOrderbook;
	}
	
	private List<Order> convertJsonToOrders(String jsonOrders) {
		ObjectMapper om = new ObjectMapper();
		try {
			return om.readValue(jsonOrders, new TypeReference<ArrayList<Order>>() {});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private String convertOrdersToJson(List<Order> orders) {
		ObjectMapper om = new ObjectMapper();
		try {
			return om.writeValueAsString(orders);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}
}
