package com.crypto.responseserver.websocket.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crypto.responseserver.websocket.component.WebsocketMsgManager;
import com.crypto.responseserver.websocket.domain.StompTopic;
import com.crypto.responseserver.websocket.domain.model.AskingPriceModel;

@Component
public class AskingPriceService {
	
	@Autowired
	private WebsocketMsgManager websocketManager;
	
	public void sendAskingPriceList(List<AskingPriceModel> models) {
		websocketManager.sendMsg(StompTopic.PRICE_LIST, models);
	}
}
