package com.crypto.responseserver.websocket.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.crypto.responseserver.RemoteServerConfig;
import com.crypto.responseserver.RemoteServerConfig.WebsocketServerApi;
import com.crypto.responseserver.core.component.RestManager;
import com.crypto.responseserver.websocket.domain.StompTopic;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class WebsocketMsgManager {
	
	@Autowired
	RestManager restManager;

	public <T> void sendMsg(StompTopic topic, T msgModel) {
		ObjectMapper om = new ObjectMapper();
		String msg = null;
		try {
			msg = om.writeValueAsString(msgModel);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		this.sendToWebsocketMsg(topic, msg);
	}

	public void sendMsg(StompTopic topic, String msg) {
		this.sendToWebsocketMsg(topic, msg);
	}

	private <T> void sendToWebsocketMsg(StompTopic topic, String msg) {
		String uri = RemoteServerConfig.getWebsocketServerUri(WebsocketServerApi.MSG_BROADCAST);
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add("topicDestination", topic.getDestination());
		params.add("msg", msg);
		restManager.post(uri, params, String.class);
	}
	
	/*@PostConstruct
	private void websocketServerHealthCheck() {
		System.out.println("############ WebsocketServer Health Check");
		sendToWebsocketMsg(StompTopic.PRICE_LIST, "WebsocketServer health check.");
	}*/
}
