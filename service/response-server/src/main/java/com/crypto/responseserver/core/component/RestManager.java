package com.crypto.responseserver.core.component;

import javax.annotation.PostConstruct;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class RestManager {
	private RestTemplate template;
	
	@PostConstruct
	public void init() {
		template = new RestTemplate();
	}
	
	public <T> ResponseEntity<T> post(String url, MultiValueMap<String, String> map, Class<T> responseClass) {
		log.info("[{}] POST -> url:{}, param:{}", this.getClass().getSimpleName(), url, map);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		ResponseEntity<T> response = template.postForEntity(url, request, responseClass);
		return response;
	}
	
	public <T> ResponseEntity<T> get(String url, MultiValueMap<String, String> map, Class<T> responseClass) {
		log.info("[{}] GET -> url:{}, param:{}", this.getClass().getSimpleName(), url, map);
		if(map == null) {
			map = new LinkedMultiValueMap<String, String>();
		}
		ResponseEntity<T> response = template.getForEntity(url, responseClass, map);
		return response;
	}
	
	
}
