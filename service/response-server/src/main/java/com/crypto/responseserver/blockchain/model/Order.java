package com.crypto.responseserver.blockchain.model;

import lombok.Getter;

@Getter
public class Order {
    private String coinName;
    private String type; // 매수,매도
    private Integer volume; //수량
    private Integer unitPrice; //거래단가
    private Integer totalPrice; //거래금액
    private ChangeType changeType; // 변경타입
    private boolean isChanage; // 변경여부
    
    public enum ChangeType {
    	ADD, MOD, NONE
    }
    
    public void setChangeType(ChangeType type) {
    	this.changeType = type;
    	this.isChanage = (type == ChangeType.NONE) ? false : true;
    }
    
}