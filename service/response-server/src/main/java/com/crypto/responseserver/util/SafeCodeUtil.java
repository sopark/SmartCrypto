package com.crypto.responseserver.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SafeCodeUtil {

    public static <T> List<T> listNotNull(List<T> list) {
        return list == null ? newArrayList() : list;
    }

    private static <E> List<E> newArrayList() {
        return new ArrayList<E>();
    }

    public static <T> Set<T> setNotNull(Set<T> set) {
        return set == null ? newHashSet() : set;
    }

    private static <E> Set<E> newHashSet() {
        return new HashSet<E>();
    }

    /*
    @SafeVarargs
    public static <T> T[] arrNotNull(T... arr) {
        return arr == null ? Lists.newArrayList().toArray(arr) : arr;
    }*/

    public static boolean isEmpty(String str) {
        return str == null || "".equals(str);
    }

    public static boolean isEmpty(List<?> list) {
        return list == null || list.isEmpty();
    }

    public static boolean isEmpty(Set<?> set) {
        return set == null || set.isEmpty();
    }

    public static boolean isEmpty(Object obj) {
        return obj == null;
    }

    public static boolean equals(Long l1, Long l2) {
        if (l1 == null && l2 == null) {
            return true;
        } else if (l1 != null && l2 != null) {
            return l1.equals(l2);
        } else {
            return false;
        }
    }
    
    public static boolean equals(Integer i1, Integer i2) {
        if (i1 == null && i2 == null) {
            return true;
        } else if (i1 != null && i2 != null) {
            return i1.equals(i2);
        } else {
            return false;
        }
    }

    public static boolean equals(String str1, String str2) {
        return str1 == null ? str2 == null : str1.equals(str2);
    }

    /*public static boolean equals(final CharSequence cs1, final CharSequence cs2) {
        if (cs1 == cs2) {
            return true;
        }
        if (cs1 == null || cs2 == null) {
            return false;
        }
        if (cs1 instanceof String && cs2 instanceof String) {
            return cs1.equals(cs2);
        }
        return regionMatches(cs1, false, 0, cs2, 0, Math.max(cs1.length(), cs2.length()));
    }

    private static boolean regionMatches(final CharSequence cs, final boolean ignoreCase, final int thisStart,
            final CharSequence substring, final int start, final int length) {
        if (cs instanceof String && substring instanceof String) {
            return ((String) cs).regionMatches(ignoreCase, thisStart, (String) substring, start, length);
        }
        int index1 = thisStart;
        int index2 = start;
        int tmpLen = length;

        while (tmpLen-- > 0) {
            char c1 = cs.charAt(index1++);
            char c2 = substring.charAt(index2++);

            if (c1 == c2) {
                continue;
            }

            if (!ignoreCase) {
                return false;
            }

            // The same check as in String.regionMatches():
            if (Character.toUpperCase(c1) != Character.toUpperCase(c2)
                    && Character.toLowerCase(c1) != Character.toLowerCase(c2)) {
                return false;
            }
        }

        return true;
    }*/

    public static boolean equalsIgnoreCase(String str1, String str2) {
        return str1 == null ? str2 == null : str1.equalsIgnoreCase(str2);
    }

}