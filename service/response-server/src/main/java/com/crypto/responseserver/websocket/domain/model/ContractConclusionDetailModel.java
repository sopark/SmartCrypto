package com.crypto.responseserver.websocket.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ContractConclusionDetailModel {
	/**
	 * 가상화폐명
	 */
	private String currencyName;
	
	/**
	 * 체결가(가격)
	 */
	private String price;
	
	/**
	 * 체결수량
	 */
	private String count;
}
