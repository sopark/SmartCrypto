package com.crypto.responseserver.schedule.job;

import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import com.crypto.responseserver.util.SafeCodeUtil;

@Component
public class QuartzJob extends QuartzJobBean {

	@Autowired
	private List<QuartzJobHandler> handlerList;

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		SafeCodeUtil.listNotNull(handlerList).forEach(handler -> handler.execute(context.getFireTime()));
	}
}