package com.crypto.responseserver.schedule.job;

import java.util.Date;

public interface QuartzJobHandler {
    void execute(Date firedTime);
}
