package com.crypto.responseserver.blockchain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PendingTransaction {
    private String pendingStatus;
    private String coinName;
    private String transactionType; // 매수,매도
    private String qty; //체결수량
    private String price; //거래단가
    private String total; //거래금액
    private String gasFee; //가스수수료
}