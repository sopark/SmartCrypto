package com.crypto.responseserver;

import java.net.URISyntaxException;

import org.apache.http.client.utils.URIBuilder;

import lombok.Getter;

public class RemoteServerConfig {
	private static final String WEBSOCKET_SERVER_SCHEME = "http";
	private static final String WEBSOCKET_SERVER_URL = "localhost";
	private static final int WEBSOCKET_SERVER_PORT = 8200;
	
	private static final String WORKER_SCHEME = "http";
	private static final String WORKER_URL = "localhost";
	private static final int WORKER_PORT = 9090;
	
	public static String getWebsocketServerUri (WebsocketServerApi api) {
		String url = null;
		try {
			url = new URIBuilder()
			.setScheme(WEBSOCKET_SERVER_SCHEME)
			.setHost(WEBSOCKET_SERVER_URL)
			.setPort(WEBSOCKET_SERVER_PORT)
			.setPath(api.getPath())
			.build().toString();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return url;
	}
	
	public static String getWorkerUri (WorkerApi api) {
		String url = null;
		try {
			url =  new URIBuilder()
			.setScheme(WORKER_SCHEME)
			.setHost(WORKER_URL)
			.setPort(WORKER_PORT)
			.setPath(api.getPath())
			.build().toString();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return url;
	} 
	
	public enum WorkerApi {
		GET_ORDER_BOOK("api/exhange/orderBook");
		@Getter
		private String path;
		
		private WorkerApi(String path) {
			this.path = path;
		}
	}
	
	public enum WebsocketServerApi {
		MSG_BROADCAST("/msg/broadcast");
		@Getter
		private String path;
		
		private WebsocketServerApi(String path) {
			this.path = path;
		}
	}
}
