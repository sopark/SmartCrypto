package com.crypto.websocketserver.domain;

import com.crypto.websocketserver.config.WebSocketBrokerConfig;

import lombok.Getter;

@Getter
public class TopicInfoModel {
	private String endPointName;
	private String messageMappingPrefix;
	private String destinationPrefix;
	private String destinationPostfix;
	private String destination;
	
	public TopicInfoModel(String destinationPostFix) {
		this.endPointName = WebSocketBrokerConfig.END_POINT_NAME;
		this.messageMappingPrefix = WebSocketBrokerConfig.MESSAGE_MAPPING_PREFIX;
		this.destinationPrefix = WebSocketBrokerConfig.DESTINATION_PREFIX;
		this.destinationPostfix = destinationPostFix;
		this.destination = this.destinationPrefix + this.destinationPostfix;
	}
}
