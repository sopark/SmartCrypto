package com.crypto.websocketserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crypto.websocketserver.service.WebSocketMsgService;

@RestController
public class WebsocketMsgController {

	@Autowired
	private WebSocketMsgService msgService;
	
	@RequestMapping("/msg/broadcast")
	public HttpStatus boradcast(String msg, String topicDestination) {
		msgService.broadcastMsg(topicDestination, msg);
		System.out.println(msg);
		return HttpStatus.OK;
	}
	
	
}
