package com.crypto.websocketserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.crypto.websocketserver.domain.WebsocketMsg;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class WebSocketMsgService {
	
	@Autowired
	private SimpMessagingTemplate template;
	
	public void broadcastMsg(String destination, String msg) {
		log.info("[{}] boradcastMsg() -> destination:{}, msg:{}", this.getClass().getSimpleName(), destination, msg);
		template.convertAndSend(destination, new WebsocketMsg(msg));
	}
	
}
