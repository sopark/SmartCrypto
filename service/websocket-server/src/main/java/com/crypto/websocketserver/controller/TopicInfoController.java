package com.crypto.websocketserver.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.crypto.websocketserver.domain.TopicInfoModel;

@RestController
public class TopicInfoController {
	
	/** SAMPLE CODE
	 * 
	@GetMapping("/topic/info/{topicName}")
	@ResponseBody
	public TopicInfoModel get{topicName}() {
		return new TopicInfoModel("/{topicName}");
	}
	*/

	@GetMapping("/topic/info/price")
	@ResponseBody
	public TopicInfoModel getPriceTopicInfo() {
		return new TopicInfoModel("/price");
	}
	
	@GetMapping("/topic/info/conclusion")
	@ResponseBody
	public TopicInfoModel getConclusionTopicInfo() {
		return new TopicInfoModel("/conclusion");
	}
	
}
